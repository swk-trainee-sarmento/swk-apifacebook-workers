<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use App\Workers\Models\Models\Post;
use Symfony\Component\HttpClient\CurlHttpClient as CurlHttpClient;

class SendPost implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    private $sentPost;

    public function __construct($post)
    {
         $this->sentPost = $post;
    }

    public function handle()
    {
        echo "Olá";
        exit;
        $idPost = $this->sentPost->id;
        $pageFacebookID = $this->sentPost->page_id;
        $accessToken = $this->sentPost->access_token;
        $message = $this->sentPost->message;

        $url = 'https://graph.facebook.com/'. $pageFacebookID .'/feed';
        $client = new CurlHttpClient();
        $response = $client->request('POST', $url, 
        ['body' => [
            'message'=> $message,
            'access_token'=> $accessToken
            ]]);

        
        $resposta = json_decode($response->getContent());
        $postFacebookID = $resposta->id;

        $post = Post::find($idPost);
        
        $post->update(['fb_id'=>$postFacebookID]);
    }
}