<?php

namespace App\Console;

use App\Jobs\SendPost;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [];

    protected function schedule(Schedule $schedule)
    {
            $schedule->call(function () {

                $posts = DB::table('Posts')
                ->join('Pages', 'Pages.id', '=', 'Posts.page_id')
                ->where('send_at','=',date('Y-m-d H:i'))
                ->select([
                'Posts.id',
                'Pages.page_id',
                'Pages.access_token',
                'Posts.message',
                ])
                ->get();
                foreach($posts as $post){
                    dispatch(new SendPost($post));
                }
                dispatch(new SendPost("Ola"));
            })
            ->description('Enviar posts para o facebook')
            ->everyMinute();
    }
}